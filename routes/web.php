<?php

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware('auth')->name('dashboard');

Route::get('/auth/github/login', function () {
    return Socialite::driver('github')->redirect();
})->middleware('guest')->name('auth.github.login');

Route::get('/auth/github/redirect', function () {
    $user = Socialite::driver('github')->user();

    $user = User::firstOrCreate(['email' => $user->email], [
        'name' => $user->name,
        'password' => Hash::make('password'),
    ]);

    \Illuminate\Support\Facades\Auth::login($user, true);

    return redirect(route('dashboard'));
})->middleware('guest');

Route::get('/auth/google/login', function () {
    return Socialite::driver('google')->redirect();
})->middleware('guest')->name('auth.google.login');

Route::get('/auth/google/redirect', function () {
    $user = Socialite::driver('google')->user();

    $user = User::firstOrCreate(['email' => $user->email], [
        'name' => $user->name,
        'password' => Hash::make('password'),
    ]);

    \Illuminate\Support\Facades\Auth::login($user, true);

    return redirect(route('dashboard'));
})->middleware('guest');

Route::get('/auth/apple/login', function () {
    return Socialite::driver('apple')->redirect();
})->middleware('guest')->name('auth.apple.login');

Route::get('/auth/apple/redirect', function () {
    $user = Socialite::driver('apple')->user();

    $user = User::firstOrCreate(['email' => $user->email], [
        'name' => $user->name,
        'password' => Hash::make('password'),
    ]);

    \Illuminate\Support\Facades\Auth::login($user, true);

    return redirect(route('dashboard'));
})->middleware('guest');
